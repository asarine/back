<?php

namespace App\Controller;

use App\Entity\User;
use Firebase\JWT\JWT;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

#[Route('/auth', name: 'auth')]
class AuthController extends AbstractController
{
    #[Route('/', name: 'auth')]
    public function index(): Response
    {
        return $this->render('auth/index.html.twig', [
            'controller_name' => 'AuthController',
        ]);
    }

    #[Route('/register', name: 'register', methods: 'POST')]
    public function register(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {
        $password = $request->get('password');
        $email = $request->get('email');
        $firstname = $request->get('firstname');
        $lastname = $request->get('lastname');


        
        $email_pattern = '/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i';
        if (!preg_match($email_pattern, $email)) {
            return $this->json([
                'error' => 'Mauvais format d\'adresse mail.',
            ]);
        }

        $email_duplication = $userRepository->findOneby(['email' => $email]);
        if ($email_duplication != null) {
            return $this->json([
                'error' => 'Cette adresse mail est déjà utilisée par un autre compte.',
            ]);
        }
        

        $user = new User();
        $user->setPassword($encoder->encodePassword($user, $password));
        $user->setEmail($email);
        $user->setFirstname($firstname);
        $user->setLastname($lastname);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return $this->json([
            'success' => 'Un compte a bien été créé pour l\'adresse mail ' . $user->getEmail() . '.'
        ]);
    }

    #[Route('/login', name: 'login', methods: 'POST')]
    public function login(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {
        $request = json_decode($request->getContent());

        $user = $userRepository->findOneBy([
                'email'=> $request->{'email'},
        ]);

        if (!$user || !$encoder->isPasswordValid($user,  $request->{'password'})) {
                return $this->json([
                    'L\'adresse mail et le mot de passe ne correspondent pas.'
                ]);
        }

        $payload = [
            "firstname" => $user->getFirstname(),
            "lastname" => $user->getLastname(),
            "email" => $user->getUsername(),
            "exp"  => (new \DateTime())->modify("+5 minutes")->getTimestamp(),
        ];


        $jwt = JWT::encode($payload, $this->getParameter('jwt_secret'), 'HS256');
        return $this->json([
            'success' => 'Les identifiants correspondent.',
            'token' => sprintf('Bearer %s', $jwt),
        ]);
    }
}